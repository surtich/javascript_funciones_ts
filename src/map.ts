export function map<A, B>(xs: A[], f: (x: A) => B): B[] {
  var ys: B[] = [];
  for (var i = 0; i < xs.length; i++) {
    ys.push(f(xs[i]));
  }
  return ys;
}

export function recuMap<A, B>(xs: A[], f: (x: A) => B): B[] {
  if (xs.length == 0) {
    return [];
  } else {
    return [f(xs[0]), ...recuMap(xs.slice(1), f)];
  }
}
