import { curry } from 'ramda';
import { map } from './map';

export var add = curry((x: number, y: number) => x + y);

export var max: (x: number, y: number) => number = (x, y) => (x > y ? x : y);

export var initial: (word: string) => string = (word) => word[0].toUpperCase();
export var concat: (x: string, y: string) => string = (x, y) => x + y;

export function maxLength(length: number, word2: string) {
  return length > word2.length ? length : word2.length;
}

export function isPair(number: number): boolean {
  return number % 2 == 0;
}

export var inc: (x: number) => number = (number) => number + 1;

export function capitalize(word: string): string {
  return word[0].toUpperCase() + word.slice(1);
}

export function isGreaterThanThree(number: number): boolean {
  return number > 3;
}

export function startWithCap(word: string): boolean {
  return word[0].toUpperCase() == word[0];
}

export var product = curry((x: number, y: number) => x * y);

export function product2(x: number) {
  return function (y: number) {
    return x * y;
  };
}

export var double = product(2);
export var triple = product(3);

export function flip<A, B, C>(f: (x: A, y: B) => C): (y: B, x: A) => C {
  return function (y: B, x: A) {
    return f(x, y);
  };
}

/*
flip(maxLength)
flip(map)

*/
