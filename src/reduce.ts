export function reduce<A, B>(xs: A[], f: (y: B, x: A) => B, init?: B): B {
  var acc = init;
  var i = 0;

  if (arguments.length == 2) {
    if (xs.length == 0) {
      throw new Error('Empty array and no initial value');
    } else {
      // @ts-ignore
      acc = xs[0];
      i = 1;
    }
  }

  for (; i < xs.length; i++) {
    // @ts-ignore
    acc = f(acc, xs[i]);
  }

  // @ts-ignore
  return acc;
}

export function recuReduce<A, B>(xs: A[], f: (y: B, x: A) => B, init: B): B {
  return xs.length == 0 ? init : recuReduce(xs.slice(1), f, f(init, xs[0]));
}

// recuReduce([1,2,3], add, 0) -> recuReduce([2,3], add, 1) -> recuReduce([3], add, 3) -> recuReduce([], add, 6) -> 6

export function recu2Reduce<A, B>(xs: A[], f: (y: B, x: A) => B, init: B): B {
  return xs.length == 0 ? init : f(recu2Reduce(xs.slice(1), f, init), xs[0]);
}

// recu2Reduce([1,2,3], add, 0) -> add(recu2Reduce([2,3], add, 0), 1) -> ?
// recu2Reduce([2,3], add, 0) -> add(recu2Reduce([3], add, 0), 2) -> ?
// recu2Reduce([3], add, 0) -> add(recu2Reduce([], add, 0), 3) -> ?
// recu2Reduce([], add, 0) -> 0

/*
export function filter<A>(xs: A[], f: (x: A) => A): A[] {
    return reduce()
}

*/

export function map<A, B>(xs: A[], f: (x: A) => B): B[] {
  return reduce(
    xs,
    function (acc, x) {
      return [...acc, f(x)];
    },
    [] as B[]
  );
}

export function filter<A>(xs: A[], f: (x: A) => boolean): A[] {
  return reduce(xs, (ys, x) => (f(x) ? [...ys, x] : ys), [] as A[]);
}
