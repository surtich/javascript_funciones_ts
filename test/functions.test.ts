import { flip, maxLength } from '../src/functions';

describe('flip tests', () => {
  test('should work with maxLength', () => {
    expect(maxLength(4, 'Andrea')).toBe(6);
    expect(maxLength(7, 'Andrea')).toBe(7);

    var maxLengthFlipped = flip(maxLength);

    expect(maxLengthFlipped('Andrea', 4)).toBe(6);
    expect(maxLengthFlipped('Andrea', 7)).toBe(7);
  });
});
