import { map, recu2Reduce, recuReduce, reduce } from '../src/reduce';
import { add, initial, isPair, max, maxLength } from '../src/functions';

function acronymize(words: string[]): string {
  return reduce(
    words,
    function (acc: string, word: string) {
      return acc + initial(word);
    },
    ''
  );
}

function recuAcronymize(words: string[]): string {
  return recuReduce(
    words,
    function (acc: string, word: string) {
      return acc + initial(word);
    },
    ''
  );
}

function recu2Acronymize(words: string[]): string {
  return recu2Reduce(
    words,
    function (acc: string, word: string) {
      return acc + initial(word);
    },
    ''
  );
}

describe('reduce tests', () => {
  test('reduce([1, 3, 4, 5, 6], add, 0) should be 19', () => {
    expect(reduce([1, 3, 4, 5, 6], add, 0)).toBe(19);
  });

  test('reduce([1, 3, 4, 5, 6], max, 0)) should be 6', () => {
    expect(reduce([], max, 0)).toBe(0);
  });

  test('reduce([], max, 0)) should be 6', () => {
    expect(reduce([], max, 0)).toBe(0);
  });

  test('reduce([], max)) should fail', () => {
    expect(() => reduce([], max)).toThrow(
      new Error('Empty array and no initial value')
    );
  });

  test('reduce([1, 3, 4, 5, 6], max) should be 6', () => {
    expect(reduce([1, 3, 4, 5, 6], add)).toBe(19);
  });

  test('reduce(["Pepe", "Ramón", "Ana", "Javier", "Luis], maxLength, 0) should be 6', () => {
    expect(
      reduce(['Pepe', 'Ramón', 'Ana', 'Javier', 'Luis'], maxLength, 0)
    ).toBe(6);
  });

  test('acronymize(["Pepe", "ramón", "Ana", "javier", "Luis"]) should be "PRAJL"', () => {
    expect(acronymize(['Pepe', 'ramón', 'Ana', 'javier', 'Luis'])).toBe(
      'PRAJL'
    );
  });

  test('reduce(new Array(100000).fill(0).map((_ , i) => i, add, 0) should be 4999950000', () => {
    expect(
      reduce(
        new Array(100000).fill(0).map((_, i) => i),
        add,
        0
      )
    ).toBe(4999950000);
  });
});

describe('recuReduce tests', () => {
  test('recuReduce([1, 3, 4, 5, 6], add, 0) should be 19', () => {
    expect(recuReduce([1, 3, 4, 5, 6], add, 0)).toBe(19);
  });

  test('recu2Reduce([1, 3, 4, 5, 6], add, 0) should be 19', () => {
    expect(recu2Reduce([1, 3, 4, 5, 6], add, 0)).toBe(19);
  });

  test('recuAcronymize(["Pepe", "ramón", "Ana", "javier", "Luis"]) should be "PRAJL"', () => {
    expect(recuAcronymize(['Pepe', 'ramón', 'Ana', 'javier', 'Luis'])).toBe(
      'PRAJL'
    );
  });

  test('recu2Acronymize(["Pepe", "ramón", "Ana", "javier", "Luis"]) should be "PRAJL"', () => {
    expect(recu2Acronymize(['Pepe', 'ramón', 'Ana', 'javier', 'Luis'])).toBe(
      'LJARP'
    );
  });
});

describe('map with reduce test', () => {
  test('map([1, 3, 4, 5, 6], isPair) should be [false, false, true, false, true]', () => {
    expect(map([1, 3, 4, 5, 6], isPair)).toEqual([
      false,
      false,
      true,
      false,
      true,
    ]);
  });
});
