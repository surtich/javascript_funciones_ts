import { map } from '../src/map';
import { add, double, isPair, product, triple } from '../src/functions';
import { filter } from '../src/filter';

describe('map tests', () => {
  test('map([1, 3, 4, 5, 6], product(2, x)) should be [2, 6, 8, 10, 12]', () => {
    expect(map([1, 3, 4, 5, 6], product(2))).toEqual([2, 6, 8, 10, 12]);
  });
  test('map([1, 3, 4], triple) should be [3, 9, 12]', () => {
    expect(map([1, 3, 4], product(3))).toEqual([3, 9, 12]);
  });

  test('map([1, 3, 4], add(1)) should be [2, 4, 5]', () => {
    expect(map([1, 3, 4], add(1))).toEqual([2, 4, 5]);
  });

  test('map([1, 3, 4], add(2)) should be [3, 5, 6]', () => {
    expect(map([1, 3, 4], add(2))).toEqual([3, 5, 6]);
  });

  test('composition test', () => {
    //@ts-ignore
    expect(map(filter(map([1, 2, 3], add(2)), isPair), product(2))).toEqual([
      8,
    ]);

    //@ts-ignore
    expect([1, 2, 3].map(add(2)).filter(isPair).map(product(2))).toEqual([8]);
  });
});

map([1, 3, 4], add(2));
